'use strict';

angular.module('myApp.bouquet', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/bouquet', {
    templateUrl: 'app/bouquet/bouquet.html',
    controller: 'BouquetCtrl'
  });
}])

.controller('BouquetCtrl', ['$scope','$window','$http',function($scope, $window, $http) {

  

  $scope.access_token = localStorage.getItem('access_token');
  if($scope.access_token === undefined || $scope.access_token === null){
      $window.location.href = "#!/login";
  }else{
      $('nav').show();
      $('.hamburger').show();
  }


  $scope.loadMore = function(){
      console.log('load More');
  }

  $scope.showLoading = false;
  $scope.showLoadingDetails = false;
  $scope.getAllBouquet = function(){
      $scope.access_token = localStorage.getItem('access_token');
      $scope.showLoading = true;

      $http({
          method : "GET",
          url : "https://stage.tooliserver.com/portal/bouquets/getAllGeneralBouquets?language=ar&page=0&pageSize=30",
          headers: {
              "authorization": "bearer "+$scope.access_token+"",
              "content-type": "application/json",
              }
          }).then(function(res) {
          console.log(res);
          $scope.bouquetList = res.data.result;
          console.log($scope.bouquetList);
        //   console.log($scope.bouquetList[0]);
        $scope.getAllChannelsInBouquet($scope.bouquetList[0].id);





        //   $scope.bouquetDetailCopy = angular.copy($scope.bouquetDetail);
        //   console.log($scope.bouquetDetailCopy);


        //   $scope.showLoading = false;


        });
  }

  $scope.getAllBouquetUpdate = function(){
    $scope.access_token = localStorage.getItem('access_token');

    $http({
        method : "GET",
        url : "https://stage.tooliserver.com/portal/bouquets/getAllGeneralBouquets?language=ar&page=0&pageSize=30",
        headers: {
            "authorization": "bearer "+$scope.access_token+"",
            "content-type": "application/json",
            }
        }).then(function(res) {
        console.log(res);
        $scope.bouquetList = res.data.result;
        console.log($scope.bouquetList);
    //   //   console.log($scope.bouquetList[0]);
    //   $scope.getAllChannelsInBouquet($scope.bouquetList[0].id);



      });
}


  $scope.getAllBouquet();

  var selectedChannels = [];
  $scope.getAllChannelsInBouquet = function(id){

    $scope.showLoadingDetails = true;

      console.log(id);
      $scope.access_token = localStorage.getItem('access_token');
          $http({
              method : "GET",
              url : "https://stage.tooliserver.com/portal/bouquets/getAllChannelsInBouquet?language=ar&id="+id+"&page=0&pageSize=10",
              headers: {
                  "authorization": "bearer "+$scope.access_token+"",
                  "content-type": "application/json",
                  }
              }).then(function(res) {
                  console.log(res);
                  $scope.bouquetDetail = res.data.result.bouquet;
                  console.log($scope.bouquetDetail);
                //   $scope.getBouquetChannels(id);

                // $scope.bouquetDetail = $scope.bouquetList[0];
                $scope.totalCount = res.data.totalCount;
                console.log($scope.totalCount);
    
                $scope.bouquetChannels = res.data.result.channels;
                console.log($scope.bouquetChannels);
                selectedChannels = [];
                $scope.bouquetChannels.forEach(function(ch){
                    console.log(ch);
                    selectedChannels.push(ch._id);
                    console.log(selectedChannels);
                });

                $scope.changeSelectedChannels(selectedChannels);
    

                
                  $scope.editBouquetDetails = false;

                  $scope.showLoading = false;
                  $scope.showLoadingDetails = false;

                  $scope.addShow = true;
                  $scope.updateStatusError = '';
                  $('.addBouquetError').hide();
                  $('.imgErrorLogo').hide();
                  $('.imgErrorCover').hide();
                  $('#imgloadLogo').val('');
                  $('#imgloadCover').val('');
              
              });
  }


  $scope.cancelAllChannelsInBouquet = function(id){


      console.log(id);
      $scope.access_token = localStorage.getItem('access_token');
          $http({
              method : "GET",
              url : "https://stage.tooliserver.com/portal/bouquets/getAllChannelsInBouquet?language=ar&id="+id+"&page=0&pageSize=10",
              headers: {
                  "authorization": "bearer "+$scope.access_token+"",
                  "content-type": "application/json",
                  }
              }).then(function(res) {
                  console.log(res);
                  $scope.bouquetDetail = res.data.result.bouquet;
                  console.log($scope.bouquetDetail);
                //   $scope.getBouquetChannels(id);

                // $scope.bouquetDetail = $scope.bouquetList[0];
                $scope.totalCount = res.data.totalCount;
                console.log($scope.totalCount);
                $scope.bouquetChannels = res.data.result.channels;
                console.log($scope.bouquetChannels);
                selectedChannels = [];
                $scope.bouquetChannels.forEach(function(ch){
                    console.log(ch);
                    selectedChannels.push(ch._id);
                    console.log(selectedChannels);
                });

                $scope.changeSelectedChannels(selectedChannels);
    

                
                $scope.editBouquetDetails = false;
                $scope.editBouquetStatusInfo = false;
                

              });
  }







//   $scope.formShow = false;
  $scope.addShow = true;
  $scope.editBouquetDetails = false;
  $scope.editBouquetStatusInfo = false;

// $scope.editBouquet = function(){
//   $scope.editBouquetDetails = true;
//   $scope.bouquetDetailCopy = angular.copy($scope.bouquetDetail);
// };
$scope.editBouquetInfo = function(){
    $scope.editBouquetDetails = true;
    $scope.bouquetDetailCopy = angular.copy($scope.bouquetDetail);
    console.log($scope.bouquetDetailCopy);
};

$scope.editBouquetStatus = function(){
    $scope.editBouquetStatusInfo = true;
    $scope.bouquetDetailCopy = angular.copy($scope.bouquetDetail);
    console.log($scope.bouquetDetailCopy);
};

$scope.cancelBouquetInfo = function(){
    $scope.editBouquetDetails = false;
    $scope.channelDetailCopy = angular.copy($scope.bouquetDetail);
    $('#imgloadLogo').val('');
    $('#imgloadCover').val('');
    $(".updateBouquetDetails").prop("disabled", false);
    $('.imgErrorLogo').hide();
    $('.imgErrorCover').hide();
    $('.nameError').hide();
    console.log($scope.channelDetailCopy);
};
$scope.cancelBouquetStatus = function(){
    $scope.editBouquetStatusInfo = false;
    $scope.updateStatusError = '';
    $scope.channelDetailCopy = angular.copy($scope.bouquetDetail);
    console.log($scope.channelDetailCopy);
};

$scope.updateBouquetInfo = function(id){

      

        var name = ''+$("#name").val()+'';

        if(name.trim() === ''){
            $('.nameError').text('Name cannot be empty');
            $('.nameError').show();
        }
        else if(!isValid( $("#name").val())) {
            $('.nameError').text('Please enter a valid Name');
            $('.nameError').show();
        }else{

            $scope.showLoadingDetails = true;
            $scope.access_token = localStorage.getItem('access_token');

            if($('#imgloadLogo').val() === '' && $('#imgloadCover').val() === ''){
                $http({

                method : "PUT",
                url : "https://stage.tooliserver.com/portal/bouquets/updateGeneralBouquet?language=ar&id="+id+"",
                headers: {
                    "authorization": "bearer "+$scope.access_token+"",
                    "content-type": "application/json",
                    },
                    data: {
                    "displayName": $('#name').val(),
                    // "status": $('#status .md-select-value .md-text').text(),
                    "isDefault": $('#IsDefault').text()
                    }
                }).then(function(res) {
                    console.log(res);
                    $scope.getAllBouquetUpdate();
                    $scope.getAllChannelsInBouquet(id);
                    
                    //   $scope.formShow = false;
                    $scope.editBouquetDetails = false;
                    $scope.editBouquetStatusInfo = false;


                
                });
            }
            // else if($('#imgloadLogo').val() === '' && $('#imgloadCover').val() === ''){

            // }
            else{
                $scope.uploadImage(id);
            }

        }
};


$scope.updateBouquetStatus = function(id){

      

    var name = ''+$("#name").val()+'';


        $scope.showLoadingDetails = true;
        $scope.access_token = localStorage.getItem('access_token');

            $http({

            method : "PUT",
            url : "https://stage.tooliserver.com/portal/bouquets/updateStatusOfGeneralBouquet?language=ar&id="+id+"",
            headers: {
                "authorization": "bearer "+$scope.access_token+"",
                "content-type": "application/json",
                },
                data: {
                // "displayName": $('#name').val(),
                "status": $('#status .md-select-value .md-text').text(),
                }
            }).then(function(res) {
                console.log(res);
                $scope.getAllBouquetUpdate();
                $scope.getAllChannelsInBouquet(id);
                
                //   $scope.formShow = false;
                $scope.editBouquetDetails = false;
                $scope.editBouquetStatusInfo = false;


            
            },function errorCallback(response) {
                console.log(response);
                console.log(response.data.message);
                $scope.updateStatusError = 'Error Occurred';
                $scope.showLoadingDetails = false;
            });

};


$scope.deleteGeneralBouquet = function(id){

        $scope.showLoadingDetails = true;
        $scope.access_token = localStorage.getItem('access_token');

            $http({

            method : "DELETE",
            url : "https://stage.tooliserver.com/portal/bouquets/deleteGeneralBouquet?language=ar&id="+id+"",
            headers: {
                "authorization": "bearer "+$scope.access_token+"",
                "content-type": "application/json",
                },
                data: {
                // "displayName": $('#name').val(),
                "status": $('#status .md-select-value .md-text').text(),
                }
            }).then(function(res) {
                console.log(res);

                $scope.getAllBouquetUpdate();
                $scope.getAllChannelsInBouquet($('.bouquetName:first').attr('id'));


                $scope.hideDeletePopup();
                //   $scope.formShow = false;
                $scope.editBouquetDetails = false;
                $scope.showLoadingDetails = false;


            
            });

};






function isValid(input) {
    var pattern = /[A-Za-z0-9&. ]/;
    return pattern.test(input);
}

$('#name').keypress(function (e) {
    var txt = String.fromCharCode(e.which);
    if (!txt.match(/[A-Za-z0-9&.]/)) {
        return false;
    }
});
$('#epgName').keypress(function (e) {
    var txt = String.fromCharCode(e.which);
    if (!txt.match(/[A-Za-z0-9&.]/)) {
        return false;
    }
});


// $("#imgload").change(function () {
//     // var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
//     var fileExtension = ['jpg', 'png'];
//     if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
//         // alert("Only formats are allowed : "+fileExtension.join(', '));
//         console.log('aaaaaaaaa');
//         // $scope.imgError = true;
//         $('.imgError').show();
//         $(".updateBouquetDetails").prop("disabled", true);

//     }else{
//         console.log('bbbbbb');
//         // $scope.imgError = false;
//         $('.imgError').hide();
//         $(".updateBouquetDetails").prop("disabled", false);
//     }
// });


$("#imgloadLogo").change(function () {
    // var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    var fileExtension = ['jpg', 'png'];
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        // alert("Only formats are allowed : "+fileExtension.join(', '));
        console.log('aaaaaaaaa');
        // $scope.imgError = true;
        $('.imgErrorLogo').show();
        $(".updateBouquetDetails").prop("disabled", true);

    }else{
        console.log('bbbbbb');
        // $scope.imgError = false;
        $('.imgErrorLogo').hide();
        $(".updateBouquetDetails").prop("disabled", false);
    }
});

$("#imgloadCover").change(function () {
    // var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    var fileExtension = ['jpg', 'png'];
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        // alert("Only formats are allowed : "+fileExtension.join(', '));
        console.log('aaaaaaaaa');
        // $scope.imgError = true;
        $('.imgErrorCover').show();
        $(".updateBouquetDetails").prop("disabled", true);

    }else{
        console.log('bbbbbb');
        // $scope.imgError = false;
        $('.imgErrorCover').hide();
        $(".updateBouquetDetails").prop("disabled", false);
    }
});

$scope.priceInfo = false;
$scope.priceInfoEdit = false;
$scope.showPriceInfo = function(){
      $scope.priceInfo = true;
  }
  $scope.hidePriceInfo = function(){
    $scope.priceInfo = false;
}
$scope.editPriceInfo = function(){
    $scope.priceInfo = false;
    $scope.priceInfoEdit = true;
}
$scope.hideEditPriceInfo = function(){
    $scope.priceInfo = true;
    $scope.priceInfoEdit = false;
}
$scope.savePriceInfo = function(){
    
}
$scope.cancelPriceInfo = function(){
    $scope.priceInfo = true;
    $scope.priceInfoEdit = false;
}



  $scope.cancelAdd = function(){
    // $scope.formShow = false;
    $scope.addShow = true;
    $scope.editBouquetDetails = false;
    $scope.addBouquetError = '';

    $('.addBouquetError').hide();
    $('.imgErrorLogo').hide();
    $('.imgErrorCover').hide();
    $('#imgloadLogo').val('');
    $('#imgloadCover').val('');


};
  
  
    $scope.goToAddBouquet = function(){
    //   $scope.formShow = true;
      $scope.addShow = false;
      $scope.editBouquetDetails = true;
    //   $scope.editBouquetStatusInfo = false;
      $scope.bouquetDetailCopy = null;
    }

    // show and hide channels list
    $scope.editChannels = false;
    $scope.editChannelsList = function(){
        $scope.editChannels = true;
        $scope.addShow = false;
        // $scope.categoryDetailCopy = angular.copy($scope.categoryDetail);
    };
    $scope.discard = function(id){
        $scope.editChannels = false;
        $scope.updateChannelsError = '';
        $scope.addShow = true;
        $scope.cancelAllChannelsInBouquet(id);

        // $scope.categoryDetailCopy = angular.copy($scope.categoryDetail);
    };










    var pageSize = 120;
    // $scope.showLoading = true
        $scope.getAllChannels = function(pageSize){
            $scope.access_token = localStorage.getItem('access_token');
            $http({
                method : "GET",
                url : "https://stage.tooliserver.com/portal/channels/getAllChannels?language=ar&page=0&pageSize="+pageSize+"",
                headers: {
                    "authorization": "bearer "+$scope.access_token+"",
                    "content-type": "application/json",
                    },
                }).then(function(res) {
                console.log(res);
                $scope.channelList = res.data.result;
                console.log($scope.channelList);

                
            });
        }
        $scope.getAllChannels(pageSize);


        $scope.changeSelectedChannels = function(selectedChannels){
            $scope.selected = selectedChannels;
            console.log($scope.selected);
            
            $scope.toggle = function (item, list) {
              var idx = list.indexOf(item);
              if (idx > -1) {
                list.splice(idx, 1);
                console.log(list);
                $scope.channelsSelected = list;
              }
              else {
                list.push(item);
                console.log(list);
                $scope.channelsSelected = list;
              }
            };
          
            $scope.exists = function (item, list) {
              return list.indexOf(item) > -1;
            };
          
            $scope.isIndeterminate = function() {
              return ($scope.selected.length !== 0 &&
                  $scope.selected.length !== $scope.channelList.length);
            };
          
            $scope.isChecked = function() {
              return $scope.selected.length === $scope.channelList.length;
            };
          
            $scope.toggleAll = function() {
              if ($scope.selected.length === $scope.channelList.length) {
                $scope.selected = [];
                console.log($scope.selected);
              } else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
                $scope.selected = $scope.channelList.slice(0);
                console.log($scope.selected);
              }
            };

        }


        



  

//   $scope.addBouquet = function(){

//     $scope.bouquetChannels = JSON.parse(localStorage.getItem('bouquetChannels'));

      
//     $scope.access_token = localStorage.getItem('access_token');
//         $http({
//             method : "POST",
//             url : "https://stage.tooliserver.com/portal/bouquets/addGeneralBouquet?language=ar",
//             headers: {
//                 "authorization": "bearer "+$scope.access_token+"",
//                 "content-type": "application/json",
//                 },
//                 data: {
//                     "displayName": $('#bouquetName').val(),
//                     "logoUrl": "/media/user/photo/photo/6-2019/a5c10505beab4cd5b6e3a170efb57870.png",
//                     "coverUrl": "/media/user/photo/photo/6-2019/a5c10505beab4cd5b6e3a170efb57870.png",
//                     "status":"Inactive",
//                     "isDefault":false
//                 }
//             }).then(function(res) {
//                 console.log(res);
//                 $scope.getAllBouquet();
//                 // $scope.editBouquetDetails = true;

//                 // $scope.formShow = false;
//                 $scope.addShow = true;
//             });
//           };



  $(document).ready(function(){
      var vHeight = window.outerHeight;
      console.log(vHeight);
      $('.bouquetContainer').height(vHeight-74);
      
  });


  $scope.uploadImage = function(id){

    // form.append("pointer", "USER_PHOTO");
    // form.append("file", $('input[type=file]')[0].files[0]);
    // form.append("file", $('input[type=file]')[1].files[0]);


    // if($('#imgloadLogo').val() != ''){
        console.log('logo');
        var form = new FormData();
        form.append("pointer", "USER_PHOTO");
        form.append("file", $('input[type=file]')[0].files[0]);
        $("#btnSubmit").prop("disabled", true);
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "https://stage.tooliserver.com/media/v1/upload-media",
            data: form,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {
    
                $("#result").text(data);
                console.log("SUCCESS : ", data);
                $("#btnSubmit").prop("disabled", false);
    
                    $http({
    
                        method : "PUT",
                        url : "https://stage.tooliserver.com/portal/bouquets/updateGeneralBouquet?language=ar&id="+id+"",
                        headers: {
                            "authorization": "bearer "+$scope.access_token+"",
                            "content-type": "application/json",
                            },
                            data: {
                                
                            "logoUrl": ""+data.result.url+"",
                            "displayName": $('#name').val(),
                            "isDefault": $('#IsDefault').text()
                            }
                        }).then(function(res) {
                            console.log(res);
                            $scope.getAllBouquetUpdate();
                            $scope.getAllChannelsInBouquet(id);
                            //   $scope.formShow = false;
                            $('#imgloadLogo').val('');
                            $('#imgloadCover').val('');
                        
                            $scope.editBouquetDetails = false;
                            $scope.editBouquetStatusInfo = false;
                        
                        });
            },
            error: function (e) {
    
                $("#result").text(e.responseText);
                console.log("ERROR : ", e);
                $("#btnSubmit").prop("disabled", false);
    
            }
        });
    
    

    // }else if($('#imgloadCover').val() != ''){
        console.log('cover');
        var form = new FormData();
        form.append("pointer", "USER_PHOTO");
        form.append("file", $('input[type=file]')[1].files[0]);
        $("#btnSubmit").prop("disabled", true);
    
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "https://stage.tooliserver.com/media/v1/upload-media",
            data: form,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {
    
                $("#result").text(data);
                console.log("SUCCESS : ", data);
                $("#btnSubmit").prop("disabled", false);
    
                    $http({
    
                        method : "PUT",
                        url : "https://stage.tooliserver.com/portal/bouquets/updateGeneralBouquet?language=ar&id="+id+"",
                        headers: {
                            "authorization": "bearer "+$scope.access_token+"",
                            "content-type": "application/json",
                            },
                            data: {
                                
                            "coverUrl": ""+data.result.url+"",
                            "displayName": $('#name').val(),
                            "isDefault": $('#IsDefault').text()
                            }
                        }).then(function(res) {
                            console.log(res);
                            $scope.getAllBouquetUpdate();
                            $scope.getAllChannelsInBouquet(id);
                            $('#imgloadLogo').val('');
                            $('#imgloadCover').val('');
                                                   //   $scope.formShow = false;
                            $scope.editBouquetDetails = false;
                            $scope.editBouquetStatusInfo = false;
                        
                        });
        
            },
            error: function (e) {
    
                $("#result").text(e.responseText);
                console.log("ERROR : ", e);
                $("#btnSubmit").prop("disabled", false);
    
            }
        });
    

    // }



};


$scope.addBouquet = function(){

    if($('#imgloadLogo').val() === ''){
        $('.imgErrorLogo').show();
    }else if($('#imgloadCover').val() === ''){
        $('.imgErrorCover').show();
    }else if($('#bouquetName').val() === ''){
        $('.addBouquetError').show();
    }else{
        $scope.showLoadingDetails = true;

        console.log('logo');
        var form = new FormData();
        form.append("pointer", "USER_PHOTO");
        form.append("file", $('input[type=file]')[0].files[0]);
        $("#btnSubmit").prop("disabled", true);
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "https://stage.tooliserver.com/media/v1/upload-media",
            data: form,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {
    
                var logoURL = data.result.url;
                $("#result").text(data);
                console.log("SUCCESS : ", data);
                $("#btnSubmit").prop("disabled", false);

                var form = new FormData();
                form.append("pointer", "USER_PHOTO");
                form.append("file", $('input[type=file]')[1].files[0]);
                $("#btnSubmit").prop("disabled", true);
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: "https://stage.tooliserver.com/media/v1/upload-media",
                    data: form,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function (data) {

                        var coverURL = data.result.url;
            
                        $("#result").text(data);
                        console.log("SUCCESS : ", data);
                        $("#btnSubmit").prop("disabled", false);


                        $scope.access_token = localStorage.getItem('access_token');

                        $http({
                            method : "POST",
                            url : "https://stage.tooliserver.com/portal/bouquets/addGeneralBouquet?language=ar",
                            headers: {
                                "authorization": "bearer "+$scope.access_token+"",
                                "content-type": "application/json",
                                },
                                data: {
                                    "displayName": $('#bouquetName').val(),
                                    "logoUrl": ""+logoURL+"",
                                    "coverUrl": ""+coverURL+"",
                                    "status":"Inactive",
                                    "isDefault":false
                                }
                            }).then(function(res) {
                                console.log(res);
                                $scope.getAllBouquet();
                                // $scope.editBouquetDetails = true;
                
                                // $scope.formShow = false;
                                $scope.addShow = true;
                            },function errorCallback(response) {
                                $('.addBouquetError').hide();
                                $('.imgErrorLogo').hide();
                                $('.imgErrorCover').hide();
                                console.log(response);
                                console.log(response.data.message);
                                $scope.addBouquetError = 'Error Occurred';
                                $scope.showLoadingDetails = false;
                            })
                
                    }
                    })
    
            },
            error: function (e) {
    
                $("#result").text(e.responseText);
                console.log("ERROR : ", e);
                $("#btnSubmit").prop("disabled", false);
    
            }
        });
                
    }

    }


$scope.updateChannelsInBouquet = function(bouquetID,channelsSelected){

    $scope.showLoadingDetails = true;

    $scope.access_token = localStorage.getItem('access_token');
    $http({
        method : "PUT",
        url : "https://stage.tooliserver.com/portal/bouquets/updateChannelsOfGeneralBouquet?language=ar&id="+bouquetID+"",
        headers: {
            "authorization": "bearer "+$scope.access_token+"",
            "content-type": "application/json",
            },
            data: {
                    "channelsIds": channelsSelected
            }
        }).then(function(res) {
        console.log(res);
        $scope.cancelAllChannelsInBouquet(bouquetID);
        $scope.editChannels = false;
        $scope.showLoadingDetails = false;
        $scope.addShow = true;
    },function errorCallback(response) {
        console.log(response);
        console.log(response.data.message);
        $scope.updateChannelsError = 'Error Occurred';
        $scope.showLoadingDetails = false;
        $scope.addShow = true;
    })
}



$scope.deletePopup = false;
$scope.showDeletePopup = function(){
    $scope.deletePopup = true;
}
$scope.hideDeletePopup = function(){
    $scope.deletePopup = false;
}


}]);