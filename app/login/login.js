


'use strict';

angular.module('myApp.login', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/login', {
    templateUrl: 'app/login/login.html',
    controller: 'LoginCtrl'
  });
}])



.controller('LoginCtrl', ['$scope','$window','$http',function($scope, $window, $http) {

    $scope.access_token = localStorage.getItem('access_token');
    if($scope.access_token === undefined || $scope.access_token === null){
        $window.location.href = "#!/login";
        $scope.hideSideBar = true;
    }else{
        $window.location.href = "#!/allChannels";
        $scope.hideSideBar = false;
    }


    var vHeight = window.outerHeight;
    console.log(vHeight);
    if(vHeight > 850){
      $('.loginForm').height(680);
      $('.loginForm').css('margin-top','100px');
      $('.LogSign').css('top','25%');
    }else if(vHeight < 728){
      $('.loginForm').height(510);
      $('.loginForm').css('margin-top','3%');
      $('.LogSign').css('top','24%');
      $('.loginBtn-overlay').css('height','300px');
    }else if(850 > vHeight > 728){
      // console.log(vHeight);
      console/localStorage('mid');
      $('.login').height(vHeight);
      $('.loginForm').height(580);
      $('.loginImg').height(vHeight);
      // $('.loginBtn-overlay').height(vHeight);
    }




function showLabel(){
  if($( "#userName" ).val() != ''){
      $('.emailInp').css('visibility','visible');
  }else if($( "#userName" ).val() === ''){
      $('.emailInp').css('visibility','hidden');
}}

function showPasswordLabel(){
  if($( "#password" ).val() != ''){
      $('.passwordInp').css('visibility','visible');
  }else if($( "#password" ).val() === ''){
      $('.passwordInp').css('visibility','hidden');
  }
}

$('.signupBtn').hover(function(){
  $('.signupBtn-overlay').css('background-image','linear-gradient(to right, rgb(177, 0, 25), rgb(154, 0, 19) 25%, rgb(95, 0, 3) 70%, rgb(85, 0, 0))');
  $('.loginBtn-overlay').css('background','#030504');
},function(){
  $('.signupBtn-overlay').css('background','#030504');
  $('.loginBtn-overlay').css('background-image','linear-gradient(to right, #b10019, #9a0013 25%, #5f0003 70%, #550000)');
});

$(".showPassword").click(function() {
  $(this).toggleClass("fa-eye fa-eye-slash");
  if ($( "#password" ).attr("type") == "password") {
      $( "#password" ).attr("type", "text");
      $( ".showPassword img" ).attr('src',"../assets/img/show.png");
  } else {
      $( "#password" ).attr("type", "password");
      $( ".showPassword img" ).attr('src',"../assets/img/hide.png");
  }
});


function isValidEmailAddress(emailAddress) {
  var pattern = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
  return pattern.test(emailAddress);
}
function isValidMobile(mobileNumber) {
  var pattern = /^[0-9]+[0-9]*$/;
  return pattern.test(mobileNumber);
}
function isValidMobileInt(mobileNumber) {
  var pattern = /^[+0-9]+[0-9]*$/;
  return pattern.test(mobileNumber);
}

if(isValidEmailAddress( $("#userName").val())) { 
  // console.log('valid email');
}
else if(isValidMobile( $("#userName").val())) {
  // console.log('valid number');
}else{
  // console.log('invalid number');
}

$scope.preventSpace = function(e) {
  console.log(e.keyCode);
  if (e.keyCode == 32) {
      // console.log('asd');
      event.preventDefault();
      // return false;
  }else if (e.keyCode == 13) {
      $scope.Login();
      // return false;
      event.preventDefault();
  }
}

$scope.Login = function(){
 
if( !isValidEmailAddress( $("#userName").val() ) ) { 
  // console.log('not valid');
}
if($("#userName").val() === '' && $("#password").val() === ''){
      $('.messageError').text('Please enter your email');
  }else  if($("#userName").val() === '' && $("#password").val() != ''){
      $('.messageError').text('Please enter your email');
  }else  if($("#userName").val() != '' && $("#password").val() === ''){
      $('.messageError').text('Please enter your password');
  }else if( isValidEmailAddress( $("#userName").val())){
      $('.loginLoad').show();
      $('.loginLoadSpan').hide();        
      $.ajax('https://stage.tooliserver.com/portal/auth/login', {
 type: "POST",
 data: {
      email :''+$("#userName").val()+'',
      password: $("#password").val()
 },
 statusCode: {
    200: function (response) {
      console.log(response);
      var savedEmail = $("#userName").val();
      var savedPassword = $("#password").val();
      var token;
      token = response.jwt_token;
      console.log(token);
    //   localStorage.setItem('savedEmail', savedEmail);
    //   localStorage.setItem('savedPassword', savedPassword);
      localStorage.setItem('access_token', token);
      console.log(localStorage.getItem('access_token'));
      $window.location.href = "#!/allChannels";
    }
 }, success: function () {
      $('.loginLoad').hide();
      $('.loginLoadSpan').show();
 },error: function(data){
  if (data.status == 400) {
      $('.loginLoad').hide();
      $('.loginLoadSpan').show();
      $('.messageError').text('Please provide email.');
      }
     else if (data.status == 401) {
      $('.loginLoad').hide();
      $('.loginLoadSpan').show();
       $('.messageError').text('Wrong username or password');
      }else if(data.status == 0){
          $('.loginLoad').hide();
          $('.loginLoadSpan').show();
          $('.messageError').text('No internet connection.');
      }
  }
});

  }else{
      $('.messageError').text('Please enter a valid email');
  }


}


}]);

