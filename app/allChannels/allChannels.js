'use strict';

angular.module('myApp.allChannels', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/allChannels', {
    templateUrl: 'app/allChannels/allChannels.html',
    controller: 'AllChannelsCtrl'
  });
}])

.controller('AllChannelsCtrl', ['$scope','$window','$http','$interval',function($scope, $window, $http ,$interval) {

    $scope.access_token = localStorage.getItem('access_token');
    if($scope.access_token === undefined || $scope.access_token === null){
        $window.location.href = "#!/login";
    }else{
        $('nav').show();
        $('.hamburger').show();
    }


    $scope.loadMore = function(){
        console.log('load More');
    }




    var pageSize = 120;
    $scope.showLoading = true
    $scope.showLoadingDetails = false;
    $scope.getAllChannels = function(pageSize){
        $scope.access_token = localStorage.getItem('access_token');
        $http({
            method : "GET",
            url : "https://stage.tooliserver.com/portal/channels/getAllChannels?language=ar&page=0&pageSize="+pageSize+"",
            headers: {
                "authorization": "bearer "+$scope.access_token+"",
                "content-type": "application/json",
                },
            }).then(function(res) {
            console.log(res);
            $scope.channelList = res.data.result;
            console.log($scope.channelList);
            console.log($scope.channelList[0]);
            $scope.channelDetail = $scope.channelList[0];

            $scope.sortType     = '';
            $scope.sortReverse  = false;
            $scope.sortByDate = '';
            $scope.sortByDate = '';
            $scope.statusFilter = '';
            $scope.comparator = ''
            $scope.search = '';
    

            $scope.channelDetailCopy = angular.copy($scope.channelDetail);
            console.log($scope.channelDetailCopy);
    
            $scope.showLoading = false;
        });
    }
    // angular.element(document.querySelector('.channels')).bind('scroll', function(){
    //     // console.log($('.channels').scrollTop());
    //     // console.log($('.channels').height());
    //     var x = pageSize;
    //     if($('.channels .channel:last').offset().top === 445.5){
    //         console.log('a7aaaaaaaaaaa');
    //         x += 20;
    //         $scope.getAllChannels(x);
    //         pageSize = x;
    //         return x;
    //     }
    //   });



    var pageSize = 120;
    $scope.getAllChannelsUpdate = function(pageSize){
        $scope.access_token = localStorage.getItem('access_token');
        $http({
            method : "GET",
            url : "https://stage.tooliserver.com/portal/channels/getAllChannels?language=ar&page=0&pageSize="+pageSize+"",
            headers: {
                "authorization": "bearer "+$scope.access_token+"",
                "content-type": "application/json",
                },
            }).then(function(res) {
            console.log(res);
            $scope.channelList = res.data.result;
        
            // console.log($scope.channelList);
            // console.log($scope.channelList[0]);
            // $scope.channelDetail = $scope.channelList[0];

            // $scope.channelDetailCopy = angular.copy($scope.channelDetail);
            // console.log($scope.channelDetailCopy);
    
            $scope.showLoading = false;
        });
    }




    $scope.getAllChannels(pageSize);

    $scope.channelDetails = function(id){
        $scope.showLoadingDetails = true;
        console.log(id);
        // $('.list').css('background-color','');
        // console.log(id);
        // $('#'+id+'').parent('.list').css('background-color','#909090');
        // console.log(id);
        
        $scope.access_token = localStorage.getItem('access_token');
            $http({
                method : "get",
                url : "https://stage.tooliserver.com/portal/channels/getChannel?language=ar&id="+id+"",
                headers: {
                    "authorization": "bearer "+$scope.access_token+"",
                    "content-type": "application/json",
                    }
                }).then(function(res) {
                    console.log(res);

                    $scope.editChannelDetails = false;
                    $scope.editChannelEpgDetails = false;
                    $scope.editChannelChatRoom = false;
                    $scope.linkedChannels = false;


                    $scope.channelDetail = res.data.result
                    console.log($scope.channelDetail);


                    $('#channelImgloaded').val('');
                    $(".updateChannelInfo").prop("disabled", false);
                    $('.channelImgErr').hide();
                    $('.nameError').hide();
            

                    $scope.showLoadingDetails = false;

                });
    }

    $scope.editChannelDetails = false;

    

    $scope.editChannelInfo = function(){
        $scope.editChannelDetails = true;
        $scope.channelDetailCopy = angular.copy($scope.channelDetail);
        console.log($scope.channelDetailCopy);
    };
    $scope.editEpgDetails = function(){
        $scope.editChannelEpgDetails = true;
        $scope.channelDetailCopy = angular.copy($scope.channelDetail);
        console.log($scope.channelDetailCopy);
    };
    $scope.editChatRoomInfo = function(){
        $scope.editChannelChatRoom = true;
        $scope.channelDetailCopy = angular.copy($scope.channelDetail);
        console.log($scope.channelDetailCopy);
    };

    $scope.cancelChannelInfo = function(){
        $scope.editChannelDetails = false;
        $scope.channelDetailCopy = angular.copy($scope.channelDetail);
        $('#channelImgloaded').val('');
        $(".updateChannelInfo").prop("disabled", false);
        $('.channelImgErr').hide();
        $('.nameError').hide();
        console.log($scope.channelDetailCopy);
    };
    $scope.cancelEpgDetails = function(){
        $scope.editChannelEpgDetails = false;
        $scope.channelDetailCopy = angular.copy($scope.channelDetail);
        $('.EpgError p').text('');
        $('.EpgError').hide();
    console.log($scope.channelDetailCopy);
    };
    $scope.cancelChatRoomInfo = function(){
        $scope.editChannelChatRoom = false;
        $scope.channelDetailCopy = angular.copy($scope.channelDetail);
        $('#channelImgloaded').val('');
        console.log($scope.channelDetailCopy);
    };


    // $scope.channelImgErr = false;

    function isValid(input) {
        var pattern = /[A-Za-z0-9&. ]/;
        return pattern.test(input);
    }

    $('#name').keypress(function (e) {
        var txt = String.fromCharCode(e.which);
        if (!txt.match(/[A-Za-z0-9&.]/)) {
            return false;
        }
    });
    $('#epgName').keypress(function (e) {
        var txt = String.fromCharCode(e.which);
        if (!txt.match(/[A-Za-z0-9&.]/)) {
            return false;
        }
    });


    $("#channelImgloaded").change(function () {
        // var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
        var fileExtension = ['jpg', 'png'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            // alert("Only formats are allowed : "+fileExtension.join(', '));
            console.log('aaaaaaaaa');
            // $scope.channelImgErr = true;
            $('.channelImgErr').show();
            $(".updateChannelInfo").prop("disabled", true);

        }else{
            console.log('bbbbbb');
            // $scope.channelImgErr = false;
            $('.channelImgErr').hide();
            $(".updateChannelInfo").prop("disabled", false);
        }
    });


    $scope.updateChannelInfo = function(id){

        
        var name = ''+$("#name").val()+'';

        if(name.trim() === ''){
            $('.nameError').text('Name cannot be empty');
            $('.nameError').show();
        }
        else if(!isValid( $("#name").val())) {
            $('.nameError').text('Please enter a valid Name');
            $('.nameError').show();
        }else{

            $scope.showLoadingDetails = true;

            $scope.access_token = localStorage.getItem('access_token');
            if($('#channelImgloaded').val() === ''){
                $http({
                    method : "put",
                    url : "https://stage.tooliserver.com/portal/channels/updateChannel?language=ar&id="+id+"",
                    headers: {
                        "authorization": "bearer "+$scope.access_token+"",
                        "content-type": "application/json",
                        },
                        data: {
                            "displayName": $('#name').val(),
                            "status": $('#status .md-select-value .md-text').text(),
                        }
                    }).then(function(res) {
                        console.log(res);
                        $scope.getAllChannelsUpdate(120);
                        $scope.channelDetails(id);

                        $scope.editChannelDetails = false;
                        $('#channelImgloaded').val('');
                        $('.nameError').hide();
                        console.log(res.data.result.modifiedDate);
                        localStorage.setItem('lastUpdate',res.data.result.modifiedDate);
                        $scope.lastUpdate = localStorage.getItem('lastUpdate');

                    });
            }else{
                $scope.uploadImage(id);
            }
    

        }

    };

    $scope.updateEpgDetails = function(id){

        
        $scope.access_token = localStorage.getItem('access_token');

        $scope.epg = {
            "name":$('#epgName').val(),
            "source":$('#sourceEPG .md-select-value .md-text').text()
        }
        if($('#supportEPG').text() === 'false'){
            console.log('Make sure supportEpg is selected');
            $('.EpgError p').text('Make sure supportEpg is selected');
            $('.EpgError').show();
        }else{
            $('.EpgError p').text('');
            $('.EpgError').hide();
            if($('#epgName').val() === '' ||$('#sourceEPG .md-select-value .md-text').text() === ''){
                $('.EpgError p').text('Name or Source cannot be empty');
                $('.EpgError').show();
            }else{
                $scope.showLoadingDetails = true;
                $http({
                    method : "put",
                    url : "https://stage.tooliserver.com/portal/channels/updateChannel?language=ar&id="+id+"",
                    headers: {
                        "authorization": "bearer "+$scope.access_token+"",
                        "content-type": "application/json",
                        },
                        data: {
                            "epg": $scope.epg,
                            "isSupportEpg": $('#supportEPG').text()
                        }
                    }).then(function(res) {
                        console.log(res);
                        $scope.getAllChannelsUpdate(120);
                        $scope.channelDetails(id);
                        $scope.editChannelEpgDetails = false;
                
                        $('#channelImgloaded').val('');
                        localStorage.setItem('lastUpdate',res.data.result.modifiedDate);
                        $scope.lastUpdate = localStorage.getItem('lastUpdate');
                    });
            }
        }
    };

    $scope.updateChatRoomInfo = function(id){

        $scope.showLoadingDetails = true;

        $scope.access_token = localStorage.getItem('access_token');

        $http({
            method : "put",
            url : "https://stage.tooliserver.com/portal/channels/updateChannel?language=ar&id="+id+"",
            headers: {
                "authorization": "bearer "+$scope.access_token+"",
                "content-type": "application/json",
                },
                data: {
                    "status": $('#status .md-select-value .md-text').text(),
                    "isChatOn": $('#chatOn').text(),
                }
            }).then(function(res) {
                console.log(res);
                $scope.getAllChannelsUpdate(120);
                $scope.channelDetails(id);
                $scope.editChannelChatRoom = false;
                        $('#channelImgloaded').val('');
                localStorage.setItem('lastUpdate',res.data.result.modifiedDate);
                $scope.lastUpdate = localStorage.getItem('lastUpdate');
            });
    };

    $scope.lastUpdate = localStorage.getItem('lastUpdate');



    $(document).ready(function(){
        var vHeight = window.outerHeight;
        console.log(vHeight);
        $('.channelsContainer').height(vHeight-74);
        
    });



        $scope.uploadImage = function(id){
            var form = new FormData();
    
            form.append("pointer", "USER_PHOTO");
            form.append("file", $('input[type=file]')[0].files[0]);
        
            $("#btnSubmit").prop("disabled", true);
    
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "https://stage.tooliserver.com/media/v1/upload-media",
                data: form,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (data) {
    
                    $("#result").text(data);
                    console.log("SUCCESS : ", data);
                    $("#btnSubmit").prop("disabled", false);

                    $http({
                        method : "put",
                        url : "https://stage.tooliserver.com/portal/channels/updateChannel?language=ar&id="+id+"",
                        headers: {
                            "authorization": "bearer "+$scope.access_token+"",
                            "content-type": "application/json",
                            },
                            data: {
                                "logoUrl": ""+data.result.url+"",
                                "displayName": $('#name').val(),
                                "status": $('#status .md-select-value .md-text').text(),
                                "isChatOn": $('#chatOn').text(),
                                "epg": $scope.epg
                            }
                        }).then(function(res) {
                            console.log(res);
                            $scope.getAllChannelsUpdate(120);
                            $scope.channelDetails(id);
                            $('#channelImgloaded').val('');
                            $scope.editChannelDetails = false;
                        });
        
    
                },
                error: function (e) {
    
                    $("#result").text(e.responseText);
                    console.log("ERROR : ", e);
                    $("#btnSubmit").prop("disabled", false);
    
                }
            });
    
        };


    $scope.filterPopup = false;
    $scope.selectedVal = 'All';
    console.log($scope.selectedVal);
    $scope.showFilterPopup = function(){
        $scope.filterPopup = true;
        console.log($scope.selectedVal);
        // if($('#statusFilter').text() != 'All'){
        //     $scope.selected = true;
        // }
    }
    $scope.hideFilterPopup = function(){
        $scope.filterPopup = false;
        console.log('asd');
    }
    
    
    // Filter
    $scope.sortType     = '';
    $scope.sortReverse  = false;
    $scope.sortByDate = '';
    $scope.filter = function(){
        console.log($scope.selectedVal);
        console.log('filter');
            $scope.sortByDate = $('.selectedRadio').text();
            $scope.statusFilter = $('#statusFilter md-select-value .md-text').text();
    
            $scope.sortReverse  = false;
    
            console.log($scope.sortByDate);
            console.log($scope.statusFilter);
            if($('#statusFilter md-select-value .md-text').text() === 'All'){
                console.log('here1');
                $scope.statusFilter = '';
                $scope.comparator = false;
                $scope.selectedVal = 'All';
                console.log($scope.selectedVal);
            }else{
                console.log('here3');
                $scope.comparator = true;
                $scope.selectedVal = $('#statusFilter').text();
                console.log($scope.selectedVal);
            }
            if($('#radioCreation').hasClass('md-checked') === true || $('#radioModified').hasClass('md-checked') === true){
                console.log('her2');
                $scope.sortReverse  = true;
                $scope.selectedVal = $('#statusFilter md-select-value .md-text').text();
                console.log($scope.selectedVal);
            }
            $scope.filterPopup = false;
    
    }
    

    // Show Date tip
    $scope.hoverIn = function(){
        this.showDate = true;
    };
    
    $scope.hoverOut = function(){
        this.showDate = false;
    };


    // Show Linked Channels
    $scope.linkedChannels = false;
    $scope.showLinkedChannels = function(){
        $scope.linkedChannels = true;
    }
    $scope.hideLinkedChannels = function(){
        $scope.linkedChannels = false;
    }


}]);
