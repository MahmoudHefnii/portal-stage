'use strict';

angular.module('myApp.category', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/category', {
    templateUrl: 'app/category/category.html',
    controller: 'CategoryCtrl'
  });
}])

.controller('CategoryCtrl', ['$scope','$window','$http',function($scope, $window, $http) {


    $scope.showLoading = true;
    $scope.showLoadingDetails = false;
    $scope.getAllcategories = function(){
        $scope.access_token = localStorage.getItem('access_token');
        $http({
            method : "GET",
            url : "https://stage.tooliserver.com/portal/categories/getAllCategories?language=ar&page=0&pageSize=20",
            headers: {
                "authorization": "bearer "+$scope.access_token+"",
                "content-type": "application/json",
                },
            }).then(function(res) {
            console.log(res);

            $scope.categoryList = res.data.result;
            console.log($scope.categoryList);
            console.log($scope.categoryList[0]);
            $scope.categoryDetail = $scope.categoryList[0];
            $scope.getCatergoryChannels($scope.categoryList[0].id);

            $scope.categoryDetailCopy = angular.copy($scope.categoryDetail);
            console.log($scope.categoryDetailCopy);

            $scope.showLoading = false;
            
        });
    }

    $scope.getAllCategoriesUpdate = function(){

        $scope.access_token = localStorage.getItem('access_token');
        $http({
            method : "GET",
            url : "https://stage.tooliserver.com/portal/categories/getAllCategories?language=ar&page=0&pageSize=20",
            headers: {
                "authorization": "bearer "+$scope.access_token+"",
                "content-type": "application/json",
                },
            }).then(function(res) {
            console.log(res);

            $scope.categoryList = res.data.result;

            $scope.showLoading = false;
            
        });


    }

    var selectedChannels = [];
    $scope.getCatergoryChannels = function(id){
                
        $scope.access_token = localStorage.getItem('access_token');
        $http({
            method : "GET",
            url : "https://stage.tooliserver.com/portal/categories/getAllChannelsInCategory?language=ar&id="+id+"&page=0&pageSize=20",
            headers: {
                "authorization": "bearer "+$scope.access_token+"",
                "content-type": "application/json",
                },
            }).then(function(res) {
            console.log(res);
            $scope.totalCount = res.data.totalCount;
            console.log($scope.totalCount);

            $scope.categoryChannels = res.data.result;
            console.log($scope.categoryChannels);
            selectedChannels = [];
            $scope.categoryChannels.forEach(function(ch){
                console.log(ch);
                selectedChannels.push(ch._id);
                console.log(selectedChannels);
            });

            $scope.changeSelectedChannels(selectedChannels);

            $scope.showLoadingDetails = false;

            
        });
    }

    $scope.getAllcategories();

    $scope.categoryDetails = function(id){
        console.log(id);
        $scope.showLoadingDetails = true;
        $scope.searchCategoryChannel = '';
        $scope.searchAllChannel = '';

        $scope.access_token = localStorage.getItem('access_token');
            $http({
                method : "get",
                url : "https://stage.tooliserver.com/portal/categories/getCategory?language=ar&id="+id+"",
                headers: {
                    "authorization": "bearer "+$scope.access_token+"",
                    "content-type": "application/json",
                    }
                }).then(function(res) {
                    console.log(res);
                    $scope.categoryDetail = res.data.result
                    console.log($scope.categoryDetail);

                    $scope.categoryDetailCopy = angular.copy($scope.categoryDetail);
                    console.log($scope.categoryDetailCopy);

                    $scope.editCategoryDetails = false;
                    $scope.getCatergoryChannels(id);

                    $('#categoryImgloaded').val('');
                    $(".updateCategoryDetails").prop("disabled", false);
                    $('.categoryImgErr').hide();
                    $('.nameError').hide();
            
                });
    }

    $scope.uploadImage = function(id){
        var form = new FormData();

        form.append("pointer", "USER_PHOTO");
        form.append("file", $('input[type=file]')[0].files[0]);
    
        $("#btnSubmit").prop("disabled", true);

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "https://stage.tooliserver.com/media/v1/upload-media",
            data: form,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {

                $("#result").text(data);
                console.log("SUCCESS : ", data);
                $("#btnSubmit").prop("disabled", false);

                $http({
                    method : "put",
                    url : "https://stage.tooliserver.com/portal/categories/updateCategory?language=ar&id="+id+"",
                    headers: {
                        "authorization": "bearer "+$scope.access_token+"",
                        "content-type": "application/json",
                        },
                        data: {
                            "name": $('#name').val(),
                            "logoUrl": ""+data.result.url+"",
                        }
                    }).then(function(res) {
                        console.log(res);
                        $scope.getAllCategoriesUpdate();
                        $scope.categoryDetails(id);
                        $('#categoryImgloaded').val('');
                        $scope.editCategoryDetails = false;
                    });
    

            },
            error: function (e) {

                $("#result").text(e.responseText);
                console.log("ERROR : ", e);
                $("#btnSubmit").prop("disabled", false);

            }
        });

    };


    $('#name').keypress(function (e) {
        var txt = String.fromCharCode(e.which);
        if (!txt.match(/[A-Za-z0-9&.]/)) {
            return false;
        }
    });



    $scope.editCategoryDetails = false;


    $scope.editInformation = function(){
        $scope.editCategoryDetails = true;
        $scope.categoryDetailCopy = angular.copy($scope.categoryDetail);
    };
    $scope.cancel = function(){
        $scope.editCategoryDetails = false;
        $scope.categoryDetailCopy = angular.copy($scope.categoryDetail);
        $('#categoryImgloaded').val('');
        $(".updateCategoryDetails").prop("disabled", false);
        $('.categoryImgErr').hide();
        $('.nameError').hide();

        console.log($scope.categoryDetailCopy);
    };


    $scope.editChannels = false;
    $scope.editChannelsList = function(){
        $scope.editChannels = true;
        // $scope.categoryDetailCopy = angular.copy($scope.categoryDetail);
    };
    $scope.discard = function(id){
        $scope.editChannels = false;
        $scope.updateChannelsError = '';
        $scope.getCatergoryChannels(id);
        // $scope.changeSelectedChannels(selectedChannels);
        // $scope.categoryDetailCopy = angular.copy($scope.categoryDetail);
    };
    

    $("#categoryImgloaded").change(function () {
        // var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
        var fileExtension = ['jpg', 'png'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            // alert("Only formats are allowed : "+fileExtension.join(', '));
            console.log('aaaaaaaaa');
            // $scope.categoryImgErr = true;
            $('.categoryImgErr').show();
            $(".updateCategoryDetails").prop("disabled", true);

        }else{
            console.log('bbbbbb');
            // $scope.categoryImgErr = false;
            $('.categoryImgErr').hide();
            $(".updateCategoryDetails").prop("disabled", false);
        }
    });


    function isValid(input) {
        var pattern = /[A-Za-z0-9&. ]/;
        return pattern.test(input);
    }


    $scope.error = false;
    $scope.updateCategoryDetails = function(id){


        var name = ''+$("#name").val()+'';

        if(name.trim() === ''){
            $('.nameError').text('Name cannot be empty');
            $('.nameError').show();
        }
        else if(!isValid( $("#name").val())) {
            $('.nameError').text('Please enter a valid Name');
            $('.nameError').show();
        }else{
            $scope.access_token = localStorage.getItem('access_token');


        if($('#categoryImgloaded').val() === ''){
            $scope.showLoadingDetails = true;
            $http({
                method : "put",
                url : "https://stage.tooliserver.com/portal/categories/updateCategory?language=ar&id="+id+"",
                headers: {
                    "authorization": "bearer "+$scope.access_token+"",
                    "content-type": "application/json",
                    },
                    data: {
                        "name": $('#name').val(),
                    }
                }).then(function(res) {
                    console.log(res);
                    $scope.getAllCategoriesUpdate();
                    $scope.categoryDetails(id);
                    $('#categoryImgloaded').val('');
                    $('.nameError').hide();
                    $scope.editCategoryDetails = false;
                });

        }else{
            $scope.uploadImage(id);
        }
    }

    };

    

    $scope.delete = function(id){
        
        $scope.access_token = localStorage.getItem('access_token');

        $http({
            method : "delete",
            url : "https://stage.tooliserver.com/portal/categories/deleteCategory?language=ar&id="+id+"",
            headers: {
                "authorization": "bearer "+$scope.access_token+"",
                "content-type": "application/json",
                }
            }).then(function(res) {
                console.log(res);
                $scope.getAllcategories();
            });
        console.log('asdsd');

    };


    var pageSize = 150;
    // $scope.showLoading = true
        $scope.getAllChannels = function(pageSize){
            $scope.access_token = localStorage.getItem('access_token');
            $http({
                method : "GET",
                url : "https://stage.tooliserver.com/portal/channels/getAllChannels?language=ar&page=0&pageSize="+pageSize+"",
                headers: {
                    "authorization": "bearer "+$scope.access_token+"",
                    "content-type": "application/json",
                    },
                }).then(function(res) {
                console.log(res);
                $scope.channelList = res.data.result;
                console.log($scope.channelList);
    
    
                $scope.showLoading = false;

            });
        }

        $scope.getAllChannels(pageSize);


        $scope.changeSelectedChannels = function(selectedChannels){
            $scope.selected = selectedChannels;
            console.log($scope.selected);
            
            $scope.toggle = function (item, list) {
              var idx = list.indexOf(item);
              if (idx > -1) {
                list.splice(idx, 1);
                console.log(list);
                $scope.channelsSelected = list;
              }
              else {
                list.push(item);
                console.log(list);
                $scope.channelsSelected = list;
              }
            };
          
            $scope.exists = function (item, list) {
              return list.indexOf(item) > -1;
            };
          
            $scope.isIndeterminate = function() {
              return ($scope.selected.length !== 0 &&
                  $scope.selected.length !== $scope.channelList.length);
            };
          
            $scope.isChecked = function() {
              return $scope.selected.length === $scope.channelList.length;
            };
          
            $scope.toggleAll = function() {
              if ($scope.selected.length === $scope.channelList.length) {
                $scope.selected = [];
                console.log($scope.selected);
              } else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
                $scope.selected = $scope.channelList.slice(0);
                console.log($scope.selected);
              }
            };

        }



        

        $scope.updateChannelsInCategory = function(categoryID,channelsSelected){

            $scope.showLoadingDetails = true;

            $scope.access_token = localStorage.getItem('access_token');
            $http({
                method : "POST",
                url : "https://stage.tooliserver.com/portal/categories/updateChannelsInCategory?language=ar",
                headers: {
                    "authorization": "bearer "+$scope.access_token+"",
                    "content-type": "application/json",
                    },
                    data: {
                            "id": categoryID,
                            "channelsIds": channelsSelected
                    }
                }).then(function(res) {
                console.log(res);
                $scope.getCatergoryChannels(categoryID);
                $scope.editChannels = false;
                $scope.showLoadingDetails = false;


            },function errorCallback(response) {
                console.log(response);
                console.log(response.data.message);
                $scope.updateChannelsError = 'Error Occurred';
                $scope.showLoadingDetails = false;
            })
        }
    

















    $(document).ready(function(){
        var vHeight = window.outerHeight;
        console.log(vHeight);
        $('.categoryContainer').height(vHeight-74);
        
    });

}]);